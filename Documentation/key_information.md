# Build Configuration

## Intent
The dashboard package is intended to run under Automative Grade Linux on a Raspberry
Pi 3 with the official touch display

See https://docs.automotivelinux.org/docs/en/master/getting_started/reference/getting-started/machines/raspberrypi.html
for details

