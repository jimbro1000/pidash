from math import sin, radians

import pygame
import pytest

from Needle import Needle

offset = 20
position = {
    "x": 0,
    "y": 1
}
width = 10
height = 10


@pytest.fixture(autouse=True)
def before_each():
    pygame.init()


def test_needle_init_uses_provided_image():
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    assert needle.needle == mock_needle


def test_needle_init_calculates_needle_length():
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    assert needle.length == width - offset


def test_needle_init_sets_relative_axis_of_rotation():
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    assert needle.axis == (0, width - offset)


def test_needle_init_sets_offset_position_of_axis():
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    assert needle.position == position


def test_needle_init_retains_needle_offset_length():
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    assert needle.axis_offset == offset


def test_rotate_returns_the_needle_rotated_by_the_given_angle(mocker):
    mocker.patch("pygame.transform.rotate")
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    rotated_needle, x, y = needle.rotate(45)
    pygame.transform.rotate.assert_called_once()


def expected_positions(target_position, angle):
    opp = offset * sin(radians(angle))
    base = opp * width / offset
    x = target_position["x"] - int(opp + base / 2)
    y = target_position["y"] + int(opp - 3 * (base / 2))
    return x, y


def test_rotate_calculates_the_screen_position_of_the_needle(mocker):
    # simple geometric test assumes angle is 45 degrees
    angle = 45
    mocker.patch("pygame.transform.rotate")
    expected_x, expected_y = expected_positions(position, angle)
    mock_needle = pygame.Surface((width, height))
    needle = Needle(mock_needle, position, offset)
    rotated_needle, x, y = needle.rotate(angle)
    assert x == expected_x
    assert y == expected_y
