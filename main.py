import json
import logging
import sys

import pygame

import StatusLight
from Speedometer import Speedometer
from StaticImage import StaticImage
from Tachometer import Tachometer


def new_dial(dial_config):
    dial_config["dial_image"] = pygame.image.load(dial_config["image"])
    dial_config["needle_image"] = pygame.image.load(dial_config["needle_image"])
    if dial_config["type"] == 'speedometer':
        return Speedometer(dial_config["dial_image"], dial_config["needle_image"], dial_config["position"],
                           dial_config["needle_offset"],
                           dial_config["max_value"], dial_config["input"])
    if dial_config["type"] == 'tachometer':
        return Tachometer(dial_config["dial_image"], dial_config["needle_image"], dial_config["position"],
                          dial_config["needle_offset"],
                          dial_config["max_value"], dial_config["input"])
    if dial_config["type"] == 'static':
        return StaticImage(dial_config["dial_image"], dial_config["needle_image"], dial_config["position"],
                           0, 0, "")
    return None


def load_config(src):
    result = {}
    try:
        with open(src, "r") as src_file:
            result = json.load(src_file)
    except FileNotFoundError:
        logging.warning("missing configuration file")
    except ValueError:
        logging.warning("invalid configuration file")
    return result


def load_images(status_config):
    blank_image = pygame.Surface((30, 30))
    result = [blank_image]
    for ref in status_config["status_images"]:
        try:
            img = pygame.image.load(ref)
            result.append(img)
        except pygame.error:
            logging.warning("invalid image file %s", ref)
            result.append(blank_image)
    return result


class Main(object):
    background = 200, 200, 200
    peak_torque = 6400
    drop_revs = 3200
    max_revs = 8000

    def __init__(self, width=800, height=480, json_file='config.json'):
        pygame.init()
        self.width, self.height = width, height
        self.screen = pygame.display.set_mode((self.width, self.height))
        config = load_config(json_file)
        dials = config["dials"]
        self.dials = []
        for dial in dials:
            gauge = new_dial(dial)
            if gauge is not None:
                self.dials.append(gauge)
        lights_array = load_images(config)
        self.board_lights = StatusLight.StatusBoard(config["status_board"], lights_array)

    def speed_calc(self, revs, gear, gears, final_drive, rotation):
        driven_revs = revs / gears[gear - 1] / final_drive
        distance = driven_revs * rotation
        return distance / 1600 * 60

    def main_loop(self):
        screen = self.screen
        revs = 800
        hazard = 1
        side = 1
        handbrake = 1
        water_temp = 1
        indicator = 1
        battery = 1
        fuel = 1
        beam = 1
        status_counter = 0
        gear = 1

        final_drive = 2.88
        gears = [4.46, 2.71, 1.96, 1.6, 1.33, 1.09]
        rotation = 1.96
        speed = self.speed_calc(revs, gear, gears, final_drive, rotation)

        ticks = pygame.time.get_ticks()

        while 1:
            self.screen.fill((11, 11, 11))
            status_update = {
                "hazard": hazard,
                "side": side,
                "handbrake": handbrake,
                "water_over_temp": water_temp,
                "indicator": indicator,
                "under_voltage": battery,
                "low_fuel": fuel,
                "full_beam": beam,
                "road_speed": speed,
                "engine_speed": revs
            }
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit()
            for dial in self.dials:
                dial.draw_gauge(screen, status_update)
            symbols = self.board_lights.refresh(status_update)
            for symbol in symbols:
                screen.blit(symbol[1], symbol[0])
            pygame.display.update()

            new_ticks = pygame.time.get_ticks()
            tick_delta = new_ticks - ticks
            ticks = new_ticks

            revs += 0.4 * tick_delta
            if revs > self.peak_torque:
                if gear == 6:
                    revs = 800
                    gear = 1
                else:
                    revs = revs / (gears[gear - 1] / gears[gear])
                    gear += 1

            speed = self.speed_calc(revs, gear, gears, final_drive, rotation)

            status_counter += 1
            if status_counter == 100:
                status_counter = 0
                if hazard == 1:
                    hazard = 0
                    if side == 1:
                        side = 0
                        if handbrake == 1:
                            handbrake = 0
                            if water_temp == 1:
                                water_temp = 0
                                if indicator == 1:
                                    indicator = 0
                                    if battery == 1:
                                        battery = 0
                                        if fuel == 1:
                                            fuel = 0
                                            if beam == 1:
                                                beam = 0
                                            else:
                                                beam = 1
                                        else:
                                            fuel = 1
                                    else:
                                        battery = 1
                                else:
                                    indicator = 1
                            else:
                                water_temp = 1
                        else:
                            handbrake = 1
                    else:
                        side = 1
                else:
                    hazard = 1


if __name__ == "__main__":
    game = Main()
    game.main_loop()
