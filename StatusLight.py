class StatusLight:
    def __init__(self, key_name, position, status_image, off_image):
        self.image_rect = status_image.get_rect().move(position["x"], position["y"])
        self.key_name = key_name
        self.off_image = off_image
        self.status_image = status_image
        self.width = self.image_rect.width
        self.height = self.image_rect.height

    def refresh(self, state):
        result = self.off_image
        if state != 0:
            result = self.status_image
        return self.image_rect, result


class StatusBoard:
    def __init__(self, lights, images):
        self.lights = []
        self.images = images
        for light_def in lights:
            key = light_def["key"]
            position = light_def["position"]
            on_image = self.images[light_def["on"]]
            off_image = self.images[light_def["off"]]
            light = StatusLight(key, position, on_image, off_image)
            self.lights.append(light)

    def refresh(self, status):
        result = []
        for light in self.lights:
            if light.key_name in status.keys():
                result.append(light.refresh(status.get(light.key_name)))
            else:
                result.append(light.refresh(0))
        return result
