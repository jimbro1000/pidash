from InstrumentData import InstrumentData


def test_when_instrument_data_is_initialised_with_zeros_it_returns_a_zeroed_dataset():
    data = InstrumentData(0, 0, 0, 0, 0)
    assert data.speed == 0
    assert data.revs == 0
    assert data.water_temperature == 0
    assert data.oil_temperature == 0
    assert len(data.signal_list) == 0


def test_when_instrument_data_is_initialised_with_a_speed_value_it_assigns_to_speed():
    speed = 100
    data = InstrumentData(0, speed, 0, 0, 0)
    assert data.speed == speed
    assert data.revs == 0
    assert data.water_temperature == 0
    assert data.oil_temperature == 0
    assert len(data.signal_list) == 0


def test_when_instrument_data_is_initialised_with_a_revs_value_it_assigns_to_revs():
    revs = 1000
    data = InstrumentData(revs, 0, 0, 0, 0)
    assert data.speed == 0
    assert data.revs == revs
    assert data.water_temperature == 0
    assert data.oil_temperature == 0
    assert len(data.signal_list) == 0


def test_when_instrument_data_is_initialised_with_a_water_temperature_value_it_assigns_to_revs():
    water = 89
    data = InstrumentData(0, 0, water, 0, 0)
    assert data.speed == 0
    assert data.revs == 0
    assert data.water_temperature == water
    assert data.oil_temperature == 0
    assert len(data.signal_list) == 0


def test_when_instrument_data_is_initialised_with_an_oil_temperature_value_it_assigns_to_revs():
    oil = 115
    data = InstrumentData(0, 0, 0, oil, 0)
    assert data.speed == 0
    assert data.revs == 0
    assert data.water_temperature == 0
    assert data.oil_temperature == oil
    assert len(data.signal_list) == 0


def test_when_instrument_data_is_initialised_with_a_signal_it_isolates_each_flag():
    signal = InstrumentData.EMW + \
             InstrumentData.SIDELIGHT + \
             InstrumentData.HEADLIGHT + \
             InstrumentData.HANDBRAKE
    data = InstrumentData(0, 0, 0, 0, signal)
    assert data.speed == 0
    assert data.revs == 0
    assert data.water_temperature == 0
    assert data.oil_temperature == 0
    assert data.signal_list[0] == InstrumentData.EMW
    assert data.signal_list[1] == InstrumentData.SIDELIGHT
    assert data.signal_list[2] == InstrumentData.HEADLIGHT
    assert data.signal_list[3] == InstrumentData.HANDBRAKE
