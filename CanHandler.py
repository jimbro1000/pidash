import json

import serial

from InstrumentData import InstrumentData


def convert_data(dataset):
    return json.loads(dataset)


class CanHandler:

    def __init__(self):
        self.ser = serial.Serial(
            port='/dev/ttyUSB0',
            baudrate=9600,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout=1
        )

    def request_data(self):
        self.ser.write("r")
        can_result = ""
        partial = "x"
        while partial != "":
            partial = self.ser.read_all()
            can_result += partial
        if can_result == "{}" or can_result == "":
            result = InstrumentData(0, 0, 0, 0, 0)
        else:
            data = convert_data(can_result)
            result = InstrumentData(data.revs, data.speed, data.water, data.oil, data.signals)
        return result
