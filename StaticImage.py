class StaticImage:
    def __init__(self, face_image, needle_image, position, offset, max_speed, field):
        self.static_image = face_image
        self.rect = self.static_image.get_rect().move(position["dial_x"], position["dial_y"])

    def draw_gauge(self, screen, status):
        screen.blit(self.static_image, self.rect)
