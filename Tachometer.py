from Needle import Needle


class Tachometer:
    max_angle = -25
    min_angle = 56

    def __init__(self, face_image, needle_image, position, offset, max_revs, field):
        self.face = face_image
        self.needle = Needle(needle_image, position, offset)
        self.gauge_rect = self.face.get_rect()
        self.gauge_rect = self.gauge_rect.move(position["dial_x"], position["dial_y"])
        self.conversion = (self.max_angle - self.min_angle) / max_revs
        self.field = field
        self.max_revs = max_revs

    def revs_to_angle(self, revs):
        return -revs * self.conversion + self.max_angle

    def draw_gauge(self, screen, status):
        revs = status[self.field]
        if revs > self.max_revs:
            revs = self.max_revs
        rotated_needle, rx, ry = self.needle.rotate(self.revs_to_angle(revs))

        rotated_rect = rotated_needle.get_rect()
        rotated_rect.x = rx
        rotated_rect.y = ry

        screen.blit(self.face, self.gauge_rect)
        screen.blit(rotated_needle, rotated_rect)
