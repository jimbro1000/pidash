from math import cos, sin, radians

import numpy as np
import pygame


class Needle:
    def __init__(self, needle_image, position, axis_offset):
        self.needle = needle_image
        needle_rect = self.needle.get_rect()
        self.length = needle_rect.width - axis_offset
        self.axis = (int(needle_rect.y / 2), self.length)
        self.position = position
        self.axis_offset = axis_offset

    def rotate(self, angle):
        # blind rotate needle around image origin
        rotated = pygame.transform.rotate(self.needle, angle)
        # build transformation to rotate around needle axis
        angle_in_radians = radians(angle)
        image_rect = self.needle.get_rect()
        cos_result = cos(angle_in_radians)
        sin_result = sin(angle_in_radians)
        transform_matrix = np.array(((cos_result, sin_result), (-sin_result, cos_result)))
        image_bounds = np.array((
            (-image_rect.width + self.axis_offset, 0 - image_rect.height / 2),
            (self.axis_offset, 0 - image_rect.height / 2),
            (-image_rect.width + self.axis_offset, image_rect.height / 2),
            (self.axis_offset, image_rect.height / 2)
        ))
        # transform image bounds
        image_bounds = image_bounds.dot(transform_matrix)
        # find rectangle bounds
        x1, y1 = int(image_bounds[0][0]), int(image_bounds[0][1])
        x2, y2 = int(image_bounds[1][0]), int(image_bounds[1][1])
        x3, y3 = int(image_bounds[2][0]), int(image_bounds[2][1])
        x4, y4 = int(image_bounds[3][0]), int(image_bounds[3][1])
        # calculate actual screen position
        x = self.position["x"] - max(x1, x2, x3, x4)
        y = self.position["y"] + min(y1, y2, y3, y4)
        return rotated, x, y
