def parse_signals(signals):
    result = list()
    value = 1
    while signals > 0:
        if signals % 2 > 0:
            result.append(value)
            signals -= 1
        value = value * 2
        signals = signals / 2
    return result


class InstrumentData:
    (EMW,
     SIDELIGHT,
     HEADLIGHT,
     HEADMAIN,
     INDICATORLEFT,
     INDICATORRIGHT,
     HANDBRAKE,
     BRAKELEVEL,
     FRONTFOG,
     REARFOG,
     BATTERY,
     OILPRESSURE,
     WATERTEMP
     ) = (1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4196)

    def __init__(self, revs, speed, water, oil, signals):
        self.revs = revs
        self.speed = speed
        self.water_temperature = water
        self.oil_temperature = oil
        self.signal_list = parse_signals(signals)
