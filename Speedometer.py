from Needle import Needle


class Speedometer:
    max_angle = 225
    min_angle = 0

    def __init__(self, face_image, needle_image, position, offset, max_speed, field):
        self.face = face_image
        self.needle = Needle(needle_image, position, offset)
        self.gauge_rect = self.face.get_rect().move(position["dial_x"], position["dial_y"])
        self.conversion = self.max_angle / max_speed
        self.field = field
        self.max_speed = max_speed

    def speed_to_angle(self, speed):
        return -speed * self.conversion + self.max_angle

    def draw_gauge(self, screen, status):
        speed = status[self.field]
        if speed > self.max_speed:
            speed = self.max_speed
        rotated_needle, rx, ry = self.needle.rotate(self.speed_to_angle(speed))

        rotated_rect = rotated_needle.get_rect()
        rotated_rect.x = rx
        rotated_rect.y = ry

        screen.blit(self.face, self.gauge_rect)
        screen.blit(rotated_needle, rotated_rect)
