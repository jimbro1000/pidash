import pygame
import pytest

import StatusLight

width = 10
height = 10
position = {"x": 0, "y": 1}
mock_image = pygame.Surface((width, height))
off_image = pygame.Surface((5, 5))
light = StatusLight.StatusLight("light", position, mock_image, off_image)


@pytest.fixture(autouse=True)
def before_each():
    pygame.init()


def test_statuslight_constructor_needs_a_key_name_position_an_on_image_and_an_off_image():
    key = "subject"
    subject = StatusLight.StatusLight(key, position, mock_image, mock_image)
    assert subject.image_rect.x == position["x"]
    assert subject.image_rect.y == position["y"]
    assert subject.width == width
    assert subject.height == height
    assert subject.key_name == key


def test_statuslight_status_accepts_an_off_signal_value_and_returns_position_and_off_image():
    rect, result = light.refresh(0)
    assert rect.x == position["x"]
    assert rect.y == position["y"]
    assert result == off_image


def test_statuslight_status_accepts_an_on_signal_value_and_returns_position_and_on_image():
    rect, result = light.refresh(1)
    assert rect.x == position["x"]
    assert rect.y == position["y"]
    assert result == mock_image


def test_statusboard_constructor_needs_an_array_of_positions_and_images():
    key = "light_key"
    lights = [{
        "key": key,
        "position": position,
        "on": 0,
        "off": 1
    }]
    images = [
        mock_image,
        off_image
    ]
    subject = StatusLight.StatusBoard(lights, images)
    expected = StatusLight.StatusLight(key, position, mock_image, off_image)
    assert len(subject.lights) == 1
    assert len(subject.images) == 2
    assert compare_lights(subject.lights[0], expected)


def test_refresh_accepts_a_set_of_key_value_pairs():
    subject = given_a_status_board()
    scenario = and_a_status_update()
    result = subject.refresh(scenario)
    assert len(result) == len(subject.lights)
    assert result[0][1] == mock_image
    assert result[1][1] == off_image


def test_if_a_key_is_missing_it_is_treated_as_off_and_included_in_result():
    subject = given_a_status_board()
    scenario = and_a_partial_status()
    result = subject.refresh(scenario)
    assert len(result) == len(subject.lights)
    assert result[0][1] == off_image
    assert result[1][1] == mock_image


def test_if_a_status_key_is_not_known_then_it_is_ignored():
    subject = given_a_status_board()
    scenario = and_a_status_with_unknown_keys()
    result = subject.refresh(scenario)
    assert len(result) == len(subject.lights)
    assert result[0][1] == off_image
    assert result[1][1] == off_image


def given_a_status_board():
    key1 = "light_key"
    key2 = "other_key"
    lights = [{
        "key": key1,
        "position": position,
        "on": 0,
        "off": 1
    }, {
        "key": key2,
        "position": position,
        "on": 0,
        "off": 1
    }]
    images = [
        mock_image,
        off_image
    ]
    return StatusLight.StatusBoard(lights, images)


def and_a_status_update():
    return {
        "light_key": 1,
        "other_key": 0
    }


def and_a_partial_status():
    return {
        "other_key": 1
    }


def and_a_status_with_unknown_keys():
    return {
        "light_key": 0,
        "not_a_key": 0
    }


def compare_lights(a, b):
    result = True
    result = result or a.key_name == b.key_name
    result = result or a.image_rect == b.image_rect
    result = result or a.width == b.width
    result = result or a.height == b.height
    result = result or a.status_image is b.status_image
    result = result or a.off_image is b.off_image
    return result
