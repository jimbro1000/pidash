import sys
from math import radians, sin, cos

import pygame
from numpy import array


def rotation_matrix(alpha):
    sin_alpha = sin(alpha)
    cos_alpha = cos(alpha)
    return (
        (cos_alpha, -sin_alpha, 0),
        (sin_alpha, cos_alpha, 0),
        (0, 0, 1)
    )


def rotate(vertices, alpha):
    return vertices.dot(rotation_matrix(alpha))


pygame.init()
size = width, height = 165, 420
background_rect = (0, 0, 165, 420)
gauge_outer_rect = (-480, -50, 640, 640)
gauge_inner_rect = (-450, -20, 580, 580)
screen = pygame.display.set_mode(size)
vertices = array(((320, 0, 0), (290, 0, 0), (283, 0, 0)))
base = 0.0
low = 800
soft = 6500
limit = 7200
hard = 7800
max = 8000
revs_increment = 100.0

ox = -160
oy = 270

base_angle = -25.0
limit_angle = 55.5

background_colour = (17, 17, 17, 0)
low_colour = (171, 179, 77)
main_colour = (74, 188, 76)
soft_colour = (255, 152, 46)
hard_colour = (224, 68, 76)
frame_color = (242, 242, 242)

rev_range = max - base
angle_range = limit_angle - base_angle
factor = angle_range / rev_range

for event in pygame.event.get():
    if event.type == pygame.QUIT:
        sys.exit()
screen.fill((128, 0, 0, 0))
pygame.draw.rect(screen, background_colour, background_rect)

# draw gradient
revs = base
increment = revs_increment / 2
while revs < low:
    angle = revs * factor + base_angle
    gradient_vertices = rotate(vertices, radians(angle))
    pygame.draw.aaline(screen, low_colour,
                       (int(gradient_vertices[0][0]) + ox, int(gradient_vertices[0][1]) + oy),
                       (int(gradient_vertices[1][0]) + ox, int(gradient_vertices[1][1]) + oy),
                       1)
    revs += increment
increment = revs_increment
while revs < soft:
    angle = revs * factor + base_angle
    gradient_vertices = rotate(vertices, radians(angle))
    pygame.draw.line(screen, main_colour,
                     (int(gradient_vertices[0][0]) + ox, int(gradient_vertices[0][1]) + oy),
                     (int(gradient_vertices[1][0]) + ox, int(gradient_vertices[1][1]) + oy),
                     3)
    revs += increment
while revs < limit:
    angle = revs * factor + base_angle
    gradient_vertices = rotate(vertices, radians(angle))
    pygame.draw.line(screen, soft_colour,
                     (int(gradient_vertices[0][0]) + ox, int(gradient_vertices[0][1]) + oy),
                     (int(gradient_vertices[1][0]) + ox, int(gradient_vertices[1][1]) + oy),
                     3)
    revs += increment
while revs < max:
    angle = revs * factor + base_angle
    gradient_vertices = rotate(vertices, radians(angle))
    pygame.draw.line(screen, hard_colour,
                     (int(gradient_vertices[0][0]) + ox, int(gradient_vertices[0][1]) + oy),
                     (int(gradient_vertices[1][0]) + ox, int(gradient_vertices[1][1]) + oy),
                     3)
    revs += increment

# draw arcs
pygame.draw.arc(screen, frame_color,
                gauge_outer_rect, radians(base_angle), radians(limit_angle),
                2)
pygame.draw.arc(screen, frame_color,
                gauge_inner_rect, radians(base_angle), radians(limit_angle),
                2)

# draw frame
angle = base_angle
while angle <= limit_angle:
    step_vertices = rotate(vertices, radians(angle))
    pygame.draw.line(screen, frame_color,
                     (int(step_vertices[0][0]) + ox, int(step_vertices[0][1]) + oy),
                     (int(step_vertices[2][0]) + ox, int(step_vertices[2][1]) + oy),
                     2)
    angle += 20

pygame.display.update()

temp_filename = "rev dial.png"
try:
    pygame.image.save(screen, temp_filename)
except:
    print("error")
sys.exit()
